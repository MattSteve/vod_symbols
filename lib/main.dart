import 'package:flutter/material.dart';
import 'package:flutter_grid_button/flutter_grid_button.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Vow of the Disciple Symbol Tracker',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: const MyHomePage(title: 'Vow of the Disciple Symbol Tracker'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  String currentEncounter = "All Symbols";

  Map<String, bool> buttonsPressed = {
    "ascendant_plane": false,
    "black_garden": false,
    "black_heart": false,
    "blank": false,
    "commune": false,
    "drink": false,
    "earth": false,
    "enter": false,
    "fleet": false,
    "grieve": false,
    "give": false,
    "guardian": false,
    "hive": false,
    "kill": false,
    "knowledge": false,
    "love": false,
    "savathun": false,
    "scorn": false,
    "stop": false,
    "tower": false,
    "skip1": false,
    "witness": false,
    "worm": false,
    "worship": false,
    "skip2": false,
    "pyramid": false,
    "darkness": false,
    "traveller": false,
    "light": false,
  };
  Map<String, bool> bonusButtonsPressed = {
    "ascendant_plane": false,
    "black_garden": false,
    "black_heart": false,
    "blank": false,
    "commune": false,
    "drink": false,
    "earth": false,
    "enter": false,
    "fleet": false,
    "grieve": false,
    "give": false,
    "guardian": false,
    "hive": false,
    "kill": false,
    "knowledge": false,
    "love": false,
    "savathun": false,
    "scorn": false,
    "stop": false,
    "tower": false,
    "skip1": false,
    "witness": false,
    "worm": false,
    "worship": false,
    "skip2": false,
    "pyramid": false,
    "darkness": false,
    "traveller": false,
    "light": false,
  };
  String bonusRooms = "";

  List<List<GridButtonItem>> _generateRowsAll() {
    List<List<GridButtonItem>> rows = [];
    List<GridButtonItem> row = [];

    for (var key in buttonsPressed.keys) {
      if (row.length == 5 || (rows.length >= 5 && row.length == 2)) {
        rows.add(row);
        row = [];
      }
      row.add(
          GridButtonItem(
              title: key,
              color: buttonsPressed[key]! ? Colors.white : Colors.black,
              child: Image.asset("assets/$key.png"),
          )
      );
    }

    if (row.isNotEmpty) {
      rows.add(row);
    }

    return rows;
  }

  List<List<GridButtonItem>> _generateRowsBonus() {
    var secretRoomButtons = ["pyramid", "give", "darkness", "traveller", "worship", "light", "stop", "guardian", "kill"];

    List<List<GridButtonItem>> rows = [];
    List<GridButtonItem> row = [];

    for (var symbol in secretRoomButtons) {
      if (row.length == 3) {
        rows.add(row);
        row = [];
      }
      row.add(
          GridButtonItem(
            title: symbol,
            color: bonusButtonsPressed[symbol]! ? Colors.white : Colors.black,
            child: Image.asset("assets/$symbol.png"),
          )
      );
    }

    if (row.isNotEmpty) {
      rows.add(row);
    }
/*
    List<GridButtonItem> lastRow = [];
    lastRow.add(
        GridButtonItem(
          title: "Rooms",
          color: Colors.black,
          child: Text(bonusRooms),
        )
    );
    rows.add(lastRow);
*/
    return rows;
  }

  List<List<GridButtonItem>> _generateRows() {
    List<List<GridButtonItem>> rows = [];

    switch (currentEncounter) {
      case "All Symbols": rows = _generateRowsAll(); break;
      case "Acquisition": rows = _generateRowsAll(); break;
      case "Caretaker": rows = _generateRowsAll(); break;
      case "Exhibition": rows = _generateRowsAll(); break;
      case "Rhulk": rows = _generateRowsAll(); break;
      case "Bonus Chest": rows = _generateRowsBonus(); break;
      default: rows = _generateRowsAll();
    }

    return rows;
  }

  void _pressButton(String value) {
    if (!value.contains("skip")) {
      switch (currentEncounter) {
        case "All Symbols": buttonsPressed[value] = buttonsPressed[value]! ? false : true; break;
        case "Acquisition": buttonsPressed[value] = buttonsPressed[value]! ? false : true; break;
        case "Caretaker": buttonsPressed[value] = buttonsPressed[value]! ? false : true; break;
        case "Exhibition": buttonsPressed[value] = buttonsPressed[value]! ? false : true; break;
        case "Rhulk": buttonsPressed[value] = buttonsPressed[value]! ? false : true; break;
        case "Bonus Chest": bonusButtonsPressed[value] = bonusButtonsPressed[value]! ? false : true; break;
        default: buttonsPressed[value] = buttonsPressed[value]! ? false : true;
      }
    }
  }

  void _reset() {
    setState(() {
      switch (currentEncounter) {
        case "All Symbols":
          for (var key in buttonsPressed.keys) {
          buttonsPressed[key] = false;
          }
          break;
        case "Acquisition":
          for (var key in buttonsPressed.keys) {
          buttonsPressed[key] = false;
          }
        break;
        case "Caretaker":
          for (var key in buttonsPressed.keys) {
            buttonsPressed[key] = false;
          }
          break;
        case "Exhibition":
          for (var key in buttonsPressed.keys) {
            buttonsPressed[key] = false;
          }
          break;
        case "Rhulk":
          for (var key in buttonsPressed.keys) {
            buttonsPressed[key] = false;
          }
          break;
        case "Bonus Chest":
          for (var key in bonusButtonsPressed.keys) {
            bonusButtonsPressed[key] = false;
          }
          break;
        default:
          for (var key in buttonsPressed.keys) {
            buttonsPressed[key] = false;
          }
      }
    });
  }

  PopupMenuItem _buildPopupMenuItem(String title) {
    currentEncounter = title;

    return PopupMenuItem(
      child:  Row(
        children: [
          Text(title),
        ],
      ),
      onTap: () {
        setState(() {
          currentEncounter = title;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          PopupMenuButton(
            itemBuilder: (context) => [
              _buildPopupMenuItem("Bonus Chest"),
              //_buildPopupMenuItem("Acquisition"),
              //_buildPopupMenuItem("Caretaker"),
              //_buildPopupMenuItem("Exhibition"),
              //_buildPopupMenuItem("Rhulk"),
              _buildPopupMenuItem("All Symbols"),
            ]
          )
        ],
      ),
      body: GridButton(
        onPressed: (dynamic value) {
            setState(() {
              _pressButton(value);
            });
          },
        items: _generateRows(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _reset();
        },
        backgroundColor: Colors.blueGrey,
        child: const Icon(Icons.refresh),
      ),
    );
  }
}
